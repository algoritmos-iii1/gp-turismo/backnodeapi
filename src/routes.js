const express = require('express');
//controllers
const excursaoController = require('./controllers/excursaoController');
const viagemController = require('./controllers/viagemController'); 

const routes = express.Router();

//metodos https
//Viagens
routes.get('/viagens', viagemController.index);
routes.post('/viagens', viagemController.create);
routes.delete('viagens/:id', viagemController.delete);
routes.put('viagens/:id', viagemController.put);

//Excursões
routes.get('/excursoes', excursaoController.index);
routes.post('/excursoes', excursaoController.create);
routes.delete('excursoes/:code', excursaoController.delete);
routes.put('excursoes/:id', excursaoController.put);


module.exports = routes;