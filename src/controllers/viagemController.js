const connection = require('../database/connection');
module.exports = {
  async index(request, response) {
    const viagens = await connection('viagens').select('*');
    return response.json(viagens)
  },

  async create(request, response) {
    const { horaPartida,
      horaChegada,
      valor,
      tipoViagem,
      cidadePartida,
      cidadeChegada
    } = request.body;
   
    const [id] = await connection('viagens').insert({
      horaPartida,
      horaChegada,
      valor,
      tipoViagem,
      cidadePartida,
      cidadeChegada,
      excursao_id
    });
    return response.json({ id })
  },

  async delete(request, response){
    const { id } = request.params;
    const index = viagens.findIndex((v) => v.id == id);
    if (index == -1) {
      response.status(400).send();
    } else {
      viagens = viagens.filter((value) => value.id != id);
      response.status(204).send();
    }
  },

  async put(request, response){
    const { id } = request.params;
    const{
      horaPartida,
      horaChegada,
      valor,
      tipoViagem,
      cidadePartida,
      cidadeChegada
    }
    const viagem = viagens.find((value) => value.id == id);
    if(!viagem){
      response.status(400).send();
    } else {
      viagem.horaPartida = horaPartida;
      viagem.horaChegada = horaChegada;
      viagem.valor = valor;
      viagem.tipoViagem = tipoViagem
      viagem.cidadePartida = cidadePartida;
      viagem.cidadeChegada = cidadeChegada;

      response.json(viagem);
    }
  },
};