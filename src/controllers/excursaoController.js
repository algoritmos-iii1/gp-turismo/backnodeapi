const connection = require('../database/connection');
const { uuid } = 'uuidv4';

module.exports = {
  /**
   * id
   * dataPartida
   * dataChegada
   * detalhesExcursoes
   */
  async index(request, response) {
    const excursoes = await connection('excursoes').select('*');
    return response.json(excursoes)
  },

  async create(request, response) {
    const { dataPartida, dataChegada, detalhesExcursoes } = request.body;
    await connection('excursoes').insert({
      id: uuid(),
      code,
      dataPartida,
      dataChegada,
      detalhesExcursoes,
    })
    return response.json({ id });
  },

  async delete(request, response) {
    const { code } = request.params;
    const index = excursoes.findIndex((v) => v.code == code);
    if (index == -1) {
      response.status(400).send();
    } else {
      excursoes = excursoes.filter((value) => value.code != code);
      response.status(204).send();
    }
  },

  async put(request, response) {
    const { id } = request.params;
    const {
      dataPartida,
      dataChegada,
      detalhesExcursoes,
    } = request.body;

    const excursao = excursoes.find((value) => value.id == id);
    if (!excursao) {
      response.status(400).send();
    } else {

      excursao.dataPartida = dataPartida;
      excursao.dataChegada = dataChegada;
      excursao.detalhesExcursoes = detalhesExcursoes;

      response.json(excursao);
    }
  },


};